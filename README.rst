################
Kakoune GDScript
################

`Kakoune <http://kakoune.org/>`_ plugin for `GDScript <https://godotengine.org/>`_
files.

Install
#######

Add ``gdscript.kak`` to your autoload dir: ``~/.config/kak/autoload/``.

By doing so, kakoune will stop to load the default autoload load folder, and
you will lost syntax highlighting for the other languages. You can restore it
by simply dropping the `rc` file for the languages you want in your autoload
folder, or by simlinking to it::

  cd ~/.config/kak/autoload/
  ln -s /usr/share/kak/rc

Usage
#####

This plugin provides highlighting and indent hooks for ``*.gd`` files.

Licence
#######

This project is licenced under both the BSD-3 Clause and the CeCILL-B licences.

Based on python.kak, thanks to its authors.
