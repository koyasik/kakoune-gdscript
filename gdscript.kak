# https://godotengine.org/
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾

# Detection
# ‾‾‾‾‾‾‾‾‾

hook global BufCreate .*[.](gd) %{
    set-option buffer filetype gdscript
}


# Highlighters & Completion
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾

add-highlighter shared/gdscript regions
add-highlighter shared/gdscript/code default-region group

# Comments format
add-highlighter shared/gdscript/comment         region '#'   '$'              fill comment

# String formats
add-highlighter shared/gdscript/mlines_string	region -match-capture ("""|''') ("""|''') regions
add-highlighter shared/gdscript/double_string   region '"'   (?<!\\)(\\\\)*"  fill string
add-highlighter shared/gdscript/single_string   region "'"   (?<!\\)(\\\\)*'  fill string

# Integer formats
add-highlighter shared/gdscript/code/ regex '(?i)\b0x[\da-f]+l?\b' 0:value
add-highlighter shared/gdscript/code/ regex '(?i)\b([1-9]\d*|0)l?\b' 0:value
# Float formats
add-highlighter shared/gdscript/code/ regex '\b\d+[eE][+-]?\d+\b' 0:value
add-highlighter shared/gdscript/code/ regex '(\b\d+)?\.\d+\b' 0:value
add-highlighter shared/gdscript/code/ regex '\b\d+\.' 0:value

evaluate-commands %sh{
    # Grammar
    values="true|false|null|void|self|PI|TAU|INF|NAN"
    meta="extends|class_name|export|tool|breakpoint"

    # Methods list from:
    # http://docs.godotengine.org/en/3.0/classes/class_object.html#class-object
    methods="_get|_get_property_list|_init|_notification|_set|add_user_signal"
    methods="${methods}|call|call_deferred|callv|can_translate_message|connect"
    methods="${methods}|disconnect|emit_signal|free|get|get_class"
    methods="${methods}|get_incoming_connections|get_indexed|get_instance_id"
    methods="${methods}|get_meta|get_meta_list|get_method_list|get_property_list"
    methods="${methods}|get_script|get_signal_connection_list|get_signal_list"
    methods="${methods}|has_meta|has_method|has_user_signal|is_blocking_signals"
    methods="${methods}|is_class|is_connected|is_queued_for_deletion|notification"
    methods="${methods}|property_list_changed_notify|set|set_block_signals|set_indexed"
    methods="${methods}|set_message_translation|set_meta|set_script|tr"

    # Keyword list from:
    # http://docs.godotengine.org/en/3.0/getting_started/scripting/gdscript/gdscript_basics.html#language
    # The keywords `true`, `falses`, `null`, `void`, `self`, `PI`, `TAU`, `INF` and `NAN are
    # in the values list instead of keywords
    # The keywords `extend`, `class_name`, `export`, `tool` and `breakpoint` are in
    #the meta list instead of keywords
    keywords="if|elif|else|for|do|while|match|switch|case|break|continue|pass|return"
    keywords="${keywords}|class|is|signal|func|static|const|enum|var|onready|setget"
    keywords="${keywords}|preload|yield|assert|remote|master|slave|sync"

    # Builtin types list from:
    # http://docs.godotengine.org/en/3.0/getting_started/scripting/gdscript/gdscript_basics.html#language
    types="bool|int|float|String|Vector2|Rect2|Vector3|Transform2D|Plane|Quat|AABB"
    types="${types}|Basis|Transform|Color|NodePath|RID|Object|Array|PoolByteArray"
    types="${types}|PoolIntArray|PoolRealArray|PoolStringArray|PoolVector2Array"
    types="${types}|PoolVector3Array|PoolColorArray|Dictionary"

    # Builtin functions list from:
    # http://docs.godotengine.org/en/3.0/classes/class_@gdscript.html
    functions="Color8|ColorN|abs|acos|asin|assert|atan|atan2|bytes2var|cartesian2polar"
    functions="${functions}|ceil|char|clamp|convert|cos|cosh|db2linear|decimals"
    functions="${functions}|dectime|deg2rad|dict2inst|ease|exp|floor|fmod|fposmod"
    functions="${functions}|funcref|hash|inst2dict|instance_from_id|inverse_lerp"
    functions="${functions}|is_inf|is_nan|len|lerp|linear2db|load|log|max|min"
    functions="${functions}|nearest_po2|parse_json|polar2cartesian|pow|preload"
    functions="${functions}|print|print_stack|printerr|printraw|prints|printt"
    functions="${functions}|rad2deg|rand_range|rand_seed|randf|randi|randomize"
    functions="${functions}|range|range_lerp|round|seed|sign|sin|sinh|sqrt|stepify"
    functions="${functions}|str|str2var|tan|tanh|to_json|type_exists|typeof"
    functions="${functions}|validate_json|var2bytes|var2str|weakref|wrapf|wrapi|yield"

    # Add the language's grammar to the static completion list
    printf %s\\n "hook global WinSetOption filetype=gdscript %{
        set-option window static_words ${values} ${meta} ${methods} ${keywords} ${types} ${functions}
    }" | tr '|' ' '

    # Highlight keywords
    printf %s "
        add-highlighter shared/gdscript/code/ regex '\b(${values})\b' 0:value
        add-highlighter shared/gdscript/code/ regex '\b(${meta})\b' 0:meta
        add-highlighter shared/gdscript/code/ regex '\bfunc\s+(${methods})\b' 1:function
        add-highlighter shared/gdscript/code/ regex '\b(${keywords})\b' 0:keyword
        add-highlighter shared/gdscript/code/ regex '\b(${functions})\b\(' 1:builtin
        add-highlighter shared/gdscript/code/ regex '\b(${types})\b' 0:type
        add-highlighter shared/gdscript/code/ regex '@[\w_]+\b' 0:attribute
    "
}

add-highlighter shared/gdscript/code/ regex (?<=[\w\s\d'"_])(<=|<<|>>|>=|<>|<|>|!=|==|\||\^|&|\+|-|\*\*|\*|//|/|%|~) 0:operator
add-highlighter shared/gdscript/code/ regex (?<=[\w\s\d'"_])((?<![=<>!])=(?![=])|[+*-]=) 0:builtin

# Commands
# ‾‾‾‾‾‾‾‾

define-command -hidden gdscript-indent-on-new-line %{
    evaluate-commands -draft -itersel %{
        # copy '#' comment prefix and following white spaces
        try %{ execute-keys -draft k <a-x> s ^\h*#\h* <ret> y jgh P }
        # preserve previous line indent
        try %{ execute-keys -draft \; K <a-&> }
        # cleanup trailing whitespaces from previous line
        try %{ execute-keys -draft k <a-x> s \h+$ <ret> d }
        # indent after line ending with :
        try %{ execute-keys -draft <space> k <a-x> <a-k> :$ <ret> j <a-gt> }
    }
}

# Initialization
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾

hook -group gdscript-highlight global WinSetOption filetype=gdscript %{ add-highlighter window/gdscript ref gdscript }

hook global WinSetOption filetype=gdscript %{
    hook window InsertChar \n -group gdscript-indent gdscript-indent-on-new-line
    # cleanup trailing whitespaces on current line insert end
    hook window ModeChange insert:.* -group gdscript-indent %{ try %{ execute-keys -draft \; <a-x> s ^\h+$ <ret> d } }
}

hook -group gdscript-highlight global WinSetOption filetype=(?!gdscript).* %{ remove-highlighter window/gdscript }

hook global WinSetOption filetype=(?!gdscript).* %{
    remove-hooks window gdscript-indent
}
